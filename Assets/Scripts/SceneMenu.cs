﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using USceneManager = UnityEngine.SceneManagement.SceneManager;

public class SceneMenu : MonoBehaviour
{
    public static SceneMenu Instance = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this);
        }
    }

    [SerializeField] private string _menuScene;

    #region CALLABLE
    public void LoadScene(int index)
    {
        StartCoroutine(IELoadScene(index));
    }

    public void LoadScene(string index)
    {
        StartCoroutine(IELoadScene(index));
    }

    public void LoadSceneAsync(int index)
    {
        StartCoroutine(IELoadSceneAsync(index));
    }

    public void LoadSceneAsync(string index)
    {
        StartCoroutine(IELoadSceneAsync(index));
    }

    #endregion

    #region LOAD SYNC

    public IEnumerator IELoadScene(int index)
    {
        int max = USceneManager.sceneCountInBuildSettings - 1;
        int curr = USceneManager.GetActiveScene().buildIndex;

        if (index > max || index < 0)
        {
            Debug.LogError("Scene not found in build index.");
            yield break;
        }

        USceneManager.LoadScene(index, UnityEngine.SceneManagement.LoadSceneMode.Single);
    }

    public IEnumerator IELoadScene(string index)
    {
        int max = USceneManager.sceneCountInBuildSettings - 1;
        int curr = USceneManager.GetActiveScene().buildIndex;

        if (index == null)
        {
            Debug.LogError("Scene not found in build index.");
            yield break;
        }

        USceneManager.LoadScene(index, UnityEngine.SceneManagement.LoadSceneMode.Single);
    }

    #endregion

    #region LOAD ASYNC

    public IEnumerator IELoadSceneAsync(int index)
    {
        int max = USceneManager.sceneCountInBuildSettings - 1;
        int curr = USceneManager.GetActiveScene().buildIndex;

        if (index > max || index < 0)
        {
            Debug.LogError("Scene not found in build index.");
            yield break;
        }

        AsyncOperation load = USceneManager.LoadSceneAsync(index, UnityEngine.SceneManagement.LoadSceneMode.Additive);

        if (load == null) yield break;

        float elapsed = 0.0f;
        const float MAX_ELAPSED = 2.0f;
        float progress = 0.0f;

        load.allowSceneActivation = false;
        Coroutine unload = StartCoroutine(IEUnloadSceneAsync(curr));

        do
        {
            progress = Mathf.Clamp01(elapsed / MAX_ELAPSED) * 1f;

            if (progress >= 0.9f && !load.allowSceneActivation)
            {
                load.allowSceneActivation = true;
            }

            yield return null;
            elapsed += Time.deltaTime;
        }
        while (!load.isDone);

        USceneManager.SetActiveScene(USceneManager.GetSceneByBuildIndex(index));

        progress = 1f;
        yield return unload;
    }

    public IEnumerator IELoadSceneAsync(string index)
    {
        int max = USceneManager.sceneCountInBuildSettings - 1;
        int curr = USceneManager.GetActiveScene().buildIndex;

        if (index == null)
        {
            Debug.LogError("Scene not found in build index.");
            yield break;
        }

        AsyncOperation load = USceneManager.LoadSceneAsync(index, UnityEngine.SceneManagement.LoadSceneMode.Additive);

        if (load == null) yield break;

        float elapsed = 0.0f;
        const float MAX_ELAPSED = 2.0f;
        float progress = 0.0f;

        load.allowSceneActivation = false;
        Coroutine unload = StartCoroutine(IEUnloadSceneAsync(curr));

        do
        {
            progress = Mathf.Clamp01(elapsed / MAX_ELAPSED) * 1f;

            if (progress >= 0.9f && !load.allowSceneActivation)
            {
                load.allowSceneActivation = true;
            }

            yield return null;
            elapsed += Time.deltaTime;
        }
        while (!load.isDone);

        USceneManager.SetActiveScene(USceneManager.GetSceneByName(index));

        progress = 1f;
        yield return unload;
    }

    #endregion

    #region UNLOAD
    public IEnumerator IEUnloadSceneAsync(int index)
    {
        int max = USceneManager.sceneCountInBuildSettings - 1;

        if (index > max || index < 0)
        {
            Debug.LogError("Scene not found in build index.");
            yield break;
        }

        while (USceneManager.GetActiveScene() == USceneManager.GetSceneByBuildIndex(index))
            yield return null;

        AsyncOperation load = USceneManager.UnloadSceneAsync(index, UnloadSceneOptions.None);

        if (load == null) yield break;

        float elapsed = 0.0f;

        do
        {
            yield return null;
            elapsed += Time.deltaTime;
        }
        while (!load.isDone);
    }

    #endregion
}
