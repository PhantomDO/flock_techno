﻿using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public enum STATE { START, SETTINGS, UPDATE };

public class ApplicationManager : MonoBehaviour
{
    #region SINGLETON
    public static ApplicationManager Instance = null;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
            SetStart();
        }
        else
        {
            Destroy(gameObject);
        }        
    }
    #endregion

    public STATE CurrentState;
    public EntityManager EntityManager;

    public GameObject SettingsPanel;
    public GameObject ToggleMenu;

    public InputField BoidsNumberField;
    public Dropdown SceneSelector;
    public Dropdown PrefabSelector;
    public Toggle ToggleJob;
    public Toggle ToggleCol;
    public Toggle ToggleTrig;
    public GameObject Keyboard;

    public List<Entity> Entities = new List<Entity>();
    public List<GameObject> Prefabs = new List<GameObject>();
    public List<string> Scenes = new List<string>();

    public int BoidsNumber { get; private set; }
    public GameObject PrefabSelected { get; private set; }
    public bool IsUsingJobs { get; private set; }
    public bool IsUsingCollider { get; private set; }
    public bool IsUsingTrigger { get; private set; }

    public void SetStart() 
    {
        // DOTS Only
        if (EntityManager != default)
        {
            NativeArray<Entity> Entities = EntityManager.GetAllEntities();
            
            foreach (var e in Entities)
            {
                EntityManager.DestroyEntity(e);
            }
            
            Entities.Dispose();
        }

        CurrentState = STATE.START;
        SettingsPanel.SetActive(true);
        ToggleMenu.SetActive(false);
        Keyboard.SetActive(true);
    }
    public void SetSettings() 
    { 
        CurrentState = STATE.SETTINGS;

        BoidsNumber = int.Parse(BoidsNumberField.text);
        PrefabSelected = Prefabs[PrefabSelector.value];

        SceneMenu.Instance.LoadScene(Scenes[SceneSelector.value]);
        Debug.LogError($"{BoidsNumber}");
    }
    public void SetUpdate()
    {
        EntityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        CurrentState = STATE.UPDATE;
        SettingsPanel.SetActive(false);
        ToggleMenu.SetActive(true);
        Keyboard.SetActive(false);
    }

    public void SetJobs()
    {
        if (ToggleJob.gameObject.activeInHierarchy)
            IsUsingJobs = ToggleJob.isOn;
        else
        {
            IsUsingJobs = !IsUsingJobs;
            StartCoroutine(ChangeToggleisOn(ToggleJob, IsUsingJobs));
        }
    }

    public void SetCollider()
    {
        if (ToggleCol.gameObject.activeInHierarchy)
            IsUsingCollider = ToggleCol.isOn;
        else
        {
            IsUsingCollider = !IsUsingCollider;
            StartCoroutine(ChangeToggleisOn(ToggleCol, IsUsingCollider));
        }
    }
    
    public void SetTrigger()
    {
        if (ToggleTrig.gameObject.activeInHierarchy)
            IsUsingCollider = ToggleTrig.isOn;
        else
        {
            IsUsingCollider = !IsUsingCollider;
            StartCoroutine(ChangeToggleisOn(ToggleTrig, IsUsingTrigger));
        }
    }

    private IEnumerator ChangeToggleisOn(Toggle toggle, bool isUsing)
    {
        while (!toggle.gameObject.activeInHierarchy) yield return null;
        yield return new WaitForEndOfFrame();
        if (toggle) toggle.isOn = isUsing;
        yield break;
    }
}
