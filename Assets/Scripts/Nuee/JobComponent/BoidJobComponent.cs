﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Burst;
using Unity.Collections;
using UnityEngine.Jobs;
using Unity.Jobs;
using Nuee.Parameters;

namespace Nuee.JobComponent
{
    [System.Serializable]
    public struct BoidJobComponent
    {
        public SetupComponentJob SetupJob;
        public SetVelocityJob SetVelocityJob;
        public SetMovementJob SetMovementJob;

        public JobHandle SetupJobHandle;
        public JobHandle SetVelocityJobHandle;
        public JobHandle SetMovementJobHandle;

        public TransformAccessArray Transforms;
        public NativeArray<Boid> Boids;

        public void Dispose()
        {
            if (Boids.IsCreated) Boids.Dispose();
            if (Transforms.isCreated) Transforms.Dispose();
        }
    }

    [BurstCompile]
    public struct SetupComponentJob : IJobParallelForTransform
    {
        [NativeDisableParallelForRestriction] public NativeArray<Boid> Boids;
        [ReadOnly] public Vector3 Target;
        public void Execute(int index, TransformAccess transform)
        {
            var boid = Boids[index];

            boid.Cohesion = boid.Avoidance = transform.position;
            boid.Alignement = transform.rotation * Vector3.forward;
            boid.Target = Target;
            boid.Mates = 1;

            Boids[index] = boid;
        }
    }

    [BurstCompile]
    public struct SetVelocityJob : IJobParallelFor
    {
        [NativeDisableParallelForRestriction] public NativeArray<Boid> Boids;
        [ReadOnly] public Vector3 Target;
        [ReadOnly] public float ViewRadius;
        [ReadOnly] public float AvoidRadius;
        [ReadOnly] public float TargetRadius;
        [ReadOnly] public float TargetForce;

        public void Execute(int index)
        {
            var boid = Boids[index];
            Vector3 position = boid.Cohesion / boid.Mates;

            // Center of scene
            if (TargetForce <= 0.0f)
            {
                boid.Target = Vector3.zero;
            }
            else
            {
                Vector3 targetOffset = Target - position;
                float sqrTarget = targetOffset.sqrMagnitude;
                float t = sqrTarget / (TargetRadius * TargetRadius);
                if (t < 0.9f) boid.Target = Vector3.zero;
                else boid.Target = targetOffset * (t * t);
            }

            for (int id = 0; id < Boids.Length; id++)
            {
                if (boid.ID != Boids[id].ID)
                {
                    var boidB = Boids[id];
                    Vector3 boidBPosition = boidB.Cohesion / boidB.Mates;
                    Vector3 boidBForward = boidB.Alignement / boidB.Mates;

                    Vector3 heading = (boidBPosition - position);
                    float sqrDst = heading.sqrMagnitude;

                    if (sqrDst < ViewRadius * ViewRadius)
                    {
                        boid.Mates += 1;
                        boid.Cohesion += boidBPosition;
                        boid.Alignement += boidBForward;

                        if (sqrDst < AvoidRadius * AvoidRadius)
                        {
                            boid.Avoidance -= (heading / sqrDst);
                        }
                    }
                }
            }

            Boids[index] = boid;
        }
    }

    [BurstCompile]
    public struct SetMovementJob : IJobParallelForTransform
    {
        [NativeDisableParallelForRestriction] public NativeArray<Boid> Boids;
        [ReadOnly] public float CohesionForce;
        [ReadOnly] public float AlignementForce;
        [ReadOnly] public float AvoidanceForce;
        [ReadOnly] public float TargetForce;
        [ReadOnly] public float MoveSpeed;
        [ReadOnly] public float DeltaTime;

        public void Execute(int index, TransformAccess transform)
        {
            var boid = Boids[index];
            var position = transform.position;
            var forward = transform.rotation * Vector3.forward;

            var cohesion = CohesionForce * Vector3.Normalize((position * boid.Mates) - boid.Cohesion);
            var alignement = AlignementForce * Vector3.Normalize(((boid.Alignement / boid.Mates) - forward));
            var avoidance = AvoidanceForce * Vector3.Normalize(boid.Avoidance - position);
            var target = TargetForce * Vector3.Normalize(boid.Target - position);

            var velocity = Vector3.Normalize(cohesion + alignement + avoidance + target);
            var nextHeading = Vector3.Normalize(forward + (DeltaTime * (velocity - forward)));

            transform.rotation = Quaternion.LookRotation(nextHeading, Vector3.up);
            transform.position = position + (nextHeading * MoveSpeed * DeltaTime);

            Boids[index] = boid;
        }
    }
}

