﻿using Unity.Entities;

namespace Nuee.Techno.ECS
{
    [System.Serializable]
    [GenerateAuthoringComponent]
    public struct ECSInit : IComponentData
    {
        public Entity Prefab;
        public int Count;

        public float SpawnRange;
    }
}
