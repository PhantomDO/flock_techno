﻿using UnityEngine;
using System.Collections;
using Unity.Entities;
using Unity.Transforms;

namespace Nuee.Techno.ECS
{
    [RequiresEntityConversion]
    public class TargetToEntity : GoToEntity
    {
        public override void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            base.Convert(entity, dstManager, conversionSystem);
            dstManager.AddComponentData(entity, new ECSBoidTarget());
            dstManager.RemoveComponent<Translation>(entity);
            dstManager.RemoveComponent<Rotation>(entity);
        }
    }
}
