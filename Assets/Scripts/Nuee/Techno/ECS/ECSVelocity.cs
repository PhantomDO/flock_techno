﻿
using Unity.Entities;
using Unity.Collections;
using Unity.Jobs;
using Unity.Burst;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;
using System.Collections.Generic;

namespace Nuee.Techno.ECS
{
    [UpdateInGroup(typeof(SimulationSystemGroup))]
    [UpdateBefore(typeof(TransformSystemGroup))]
    public class ECSVelocity : SystemBase
    {
        private EntityQuery _queryBoids;
        private EntityQuery _queryTargets;
        private List<ECSParameters> _parameters = new List<ECSParameters>();

        protected override void OnCreate()
        {
            // Une query utilisé pour trouver toutes les Entitées disposant de certain composants
            // Ici les composant définissant les Boids 
            _queryBoids = GetEntityQuery(new EntityQueryDesc
            {
                All = new[]
                {
                ComponentType.ReadOnly<ECSParameters>(),
                ComponentType.ReadWrite<ECSBoid>(),
                ComponentType.ReadWrite<LocalToWorld>()
            },
            });

            // /* SECURITÉ */ Force la query à être update avant le OnUpdate()
            RequireForUpdate(_queryBoids);
        }

        protected override void OnUpdate()
        {
            // Crée une requette à la Query cherchant les composant de type Target
            var targetCount = _queryTargets.CalculateEntityCount();

            // Récupère tout les SharedComponent de type <ECSParameters> et les stocks dans une list
            EntityManager.GetAllUniqueSharedComponentData(_parameters);

            for (int v = 0; v < _parameters.Count; v++)
            {
                var parameters = _parameters[v];

                // Set le parameters de la loop à la query pour une recherche + precise
                _queryBoids.AddSharedComponentFilter(parameters);

                int boidCount = _queryBoids.CalculateEntityCount();
                if (boidCount == 0) _queryBoids.ResetFilter();

                // Initialisation de tableau interne à la fonction
                // Ils serviront de stockage aux différentes proprietées des Boids et Target
                var boidIDs = new NativeArray<int>(boidCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
                var boidMates = new NativeArray<int>(boidCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
                var boidCohesions = new NativeArray<float3>(boidCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
                var boidAlignements = new NativeArray<float3>(boidCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
                var boidAvoidances = new NativeArray<float3>(boidCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
                var boidFollows = new NativeArray<float3>(boidCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);

                var targetIDs = new NativeArray<int>(boidCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);
                var targetPositions = new NativeArray<float3>(targetCount, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);

                // Initialition des 3 mouvement requis pour l'algorithmes
                // A partir du filtres défini plus haut, executer en multithread
                var initBoidMovementJobHandle = Entities
                .WithSharedComponentFilter(parameters)
                .WithName("InitBoidMovementJob")
                .ForEach((int entityInQueryIndex, in LocalToWorld localToWorld) =>
                {
                    boidCohesions[entityInQueryIndex] = boidAvoidances[entityInQueryIndex] = localToWorld.Position;
                    boidAlignements[entityInQueryIndex] = localToWorld.Forward;
                })
                .ScheduleParallel(Dependency);

                var copyTargetPositionJobHandle = Entities
                .WithName("CopyTargetPositionJob")
                .WithAll<ECSBoidTarget>()
                .WithStoreEntityQueryInField(ref _queryTargets)
                .ForEach((int entityInQueryIndex, in LocalToWorld localToWorld) =>
                {
                    targetPositions[entityInQueryIndex] = localToWorld.Position;
                })
                .ScheduleParallel(Dependency);

                // Initialise le tableau des Mates à 1, utile pour retrouver le boids utilise par la suite
                var initialBoidMatesJob = new MemsetNativeArray<int>
                {
                    Source = boidMates,
                    Value = 1
                };
                var initBoidMatesJobHandle = initialBoidMatesJob.Schedule(boidCount, 64, Dependency);

                // Combine les jobs en parralèles avec le précedant pour forcer l'update des jobs avant la suite de la methode
                var initMovementJobHandle = JobHandle.CombineDependencies(initBoidMovementJobHandle, initBoidMatesJobHandle);
                var mergeJobHandle = JobHandle.CombineDependencies(initMovementJobHandle, copyTargetPositionJobHandle);

                // Job effectuant l'algorithme de calcul de la nouvelle direction à prendre
                var calculVelocityJob = new CalculNextDirection
                {
                    BoidIDs = boidIDs,
                    TargetIDs = targetIDs,
                    BoidMates = boidMates,
                    BoidCohesions = boidCohesions,
                    BoidAlignements = boidAlignements,
                    BoidAvoidances = boidAvoidances,
                    BoidFollows = boidFollows,
                    TargetPositions = targetPositions,

                    Params = parameters,
                };
                var mergeBoidsJobHandle = calculVelocityJob.Schedule(boidCount, 64, mergeJobHandle);

                // Ici on prend touts les mouvements calculés précédement pour les normalisé
                // On multiplie ensuite ces mouvement par leurs forces défini dans le sharedComponent
                // Enfin on override la position et rotation des boids 
                float deltaTime = Time.DeltaTime;
                var setVelocityJobHandle = Entities
                .WithName("SetVelocityJob")
                .WithSharedComponentFilter(parameters)
                .WithReadOnly(boidIDs)
                .WithReadOnly(boidMates)
                .WithReadOnly(boidAlignements)
                .WithReadOnly(boidCohesions)
                .WithReadOnly(boidAvoidances)
                .WithReadOnly(targetIDs)
                .WithReadOnly(targetPositions)
                .ForEach((int entityInQueryIndex, ref LocalToWorld localToWorld) =>
                {
                    var forward = localToWorld.Forward;
                    var position = localToWorld.Position;
                    var boidID = boidIDs[entityInQueryIndex];
                    var mates = boidMates[boidID];
                    var alignement = boidAlignements[boidID];
                    var cohesion = boidCohesions[boidID];
                    var avoidance = boidAvoidances[boidID];
                    var targetID = targetIDs[boidID];
                    var targetPosition = targetPositions[targetID];

                    var alignementResult = parameters.AlignementForce * (math.normalizesafe(alignement / mates) - forward);
                    var cohesionResult = parameters.CohesionForce * (math.normalizesafe(position * mates) - cohesion);
                    var avoidanceResult = parameters.AvoidanceForce * math.normalizesafe(avoidance - position);
                    var targetResult = parameters.AvoidanceForce * math.normalizesafe(targetPosition - position);

                    var velocity = math.normalizesafe(alignementResult + cohesionResult + avoidanceResult + targetResult);
                    var nextHeading = math.normalizesafe(forward + (deltaTime * (velocity - forward)));

                    localToWorld = new LocalToWorld
                    {
                        Value = float4x4.TRS
                        (
                            new float3(localToWorld.Position + (nextHeading * parameters.MoveSpeed * deltaTime)),
                            quaternion.LookRotationSafe(nextHeading, math.up()),
                            new float3(1.0f, 1.0f, 1.0f)
                        )
                    };
                }).ScheduleParallel(mergeBoidsJobHandle);

                // Une fois le dernier Job en parralèles effectuer on Dispose les tableau de la methodes pour eviter le memory leak
                Dependency = setVelocityJobHandle;
                var disposeJobHandle = boidIDs.Dispose(Dependency);
                disposeJobHandle = JobHandle.CombineDependencies(disposeJobHandle, boidMates.Dispose(Dependency));
                disposeJobHandle = JobHandle.CombineDependencies(disposeJobHandle, boidAlignements.Dispose(Dependency));
                disposeJobHandle = JobHandle.CombineDependencies(disposeJobHandle, boidAvoidances.Dispose(Dependency));
                disposeJobHandle = JobHandle.CombineDependencies(disposeJobHandle, boidCohesions.Dispose(Dependency));
                disposeJobHandle = JobHandle.CombineDependencies(disposeJobHandle, boidFollows.Dispose(Dependency));
                disposeJobHandle = JobHandle.CombineDependencies(disposeJobHandle, targetIDs.Dispose(Dependency));
                disposeJobHandle = JobHandle.CombineDependencies(disposeJobHandle, targetPositions.Dispose(Dependency));

                _queryBoids.AddDependency(Dependency);
                _queryBoids.ResetFilter();
            }
            _parameters.Clear();
        }

        [BurstCompile]
        private struct CalculNextDirection : IJobParallelFor
        {
            [NativeDisableParallelForRestriction] public NativeArray<int> BoidIDs;
            [NativeDisableParallelForRestriction] public NativeArray<int> TargetIDs;
            [NativeDisableParallelForRestriction] public NativeArray<int> BoidMates;
            [NativeDisableParallelForRestriction] public NativeArray<float3> BoidCohesions;
            [NativeDisableParallelForRestriction] public NativeArray<float3> BoidAlignements;
            [NativeDisableParallelForRestriction] public NativeArray<float3> BoidAvoidances;
            [NativeDisableParallelForRestriction] public NativeArray<float3> BoidFollows;
            [ReadOnly] public NativeArray<float3> TargetPositions;
            [ReadOnly] public ECSParameters Params;

            public void Nearest(NativeArray<float3> targets, float3 position, out int nearestIndex, out float nearestDist)
            {
                nearestIndex = 0;
                nearestDist = math.lengthsq(position - targets[0]);

                for (int i = 0; i < targets.Length; i++)
                {
                    float3 targetPos = targets[i];
                    float distance = math.lengthsq(targetPos - position);
                    bool nearest = distance < nearestDist;

                    nearestDist = math.select(nearestDist, distance, nearest);
                    nearestIndex = math.select(nearestIndex, i, nearest);
                }

                nearestDist = math.sqrt(nearestDist);
            }

            public void Execute(int index)
            {
                float3 position = BoidCohesions[index] / BoidMates[index];

                // Target
                Nearest(TargetPositions, position, out int nearestID, out float nearestDist);
                TargetIDs[index] = nearestID;

                //// Center of scene
                //if (Params.AvoidanceForce <= 0.0f)
                //{
                //    BoidFollows[index] = float3.zero;
                //}
                //else
                //{
                //    float3 targetOffset = TargetPositions[nearestID] - position;
                //    float sqrTarget = math.lengthsq(targetOffset);
                //    float t = sqrTarget / (8 * 8);
                //    if (t < 0.9f) BoidFollows[index] = float3.zero;
                //    else BoidFollows[index] = targetOffset * (t * t);
                //}

                for (int id = 0; id < BoidIDs.Length; id++)
                {
                    if (BoidIDs[index] != BoidIDs[id])
                    {
                        float3 boidBPosition = BoidCohesions[id] / BoidMates[id];
                        float3 boidBForward = BoidAlignements[id] / BoidMates[id];

                        float3 heading = (boidBPosition - position);
                        float sqrDst = math.lengthsq(heading);

                        if (sqrDst < Params.ViewRadius * Params.ViewRadius)
                        {
                            BoidMates[index] += 1;
                            BoidCohesions[index] += boidBPosition;
                            BoidAlignements[index] += boidBForward;

                            if (sqrDst < Params.AvoidRadius * Params.AvoidRadius)
                            {
                                BoidAvoidances[index] -= (heading / sqrDst);
                            }
                        }
                    }
                }

                BoidIDs[index] = index;
            }
        }
    }
}
