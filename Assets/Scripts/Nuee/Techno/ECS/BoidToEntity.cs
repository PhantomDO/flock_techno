﻿using UnityEngine;
using System.Collections;
using Unity.Entities;
using Unity.Transforms;

namespace Nuee.Techno.ECS
{
    [RequiresEntityConversion]
    public class BoidToEntity : GoToEntity
    {
        public override void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            base.Convert(entity, dstManager, conversionSystem);
            //ECSInit EcsInit = dstManager.GetComponentData<ECSInit>(prefabToEntity);
            //EcsInit.Prefab = prefabToEntity;
            //EcsInit.Count = ApplicationManager.Instance.BoidsNumber;

            dstManager.RemoveComponent<Translation>(entity);
            dstManager.RemoveComponent<Rotation>(entity);
        }
    }
}
