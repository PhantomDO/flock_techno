﻿using Unity.Entities;
using Unity.Transforms;

namespace Nuee.Techno.ECS
{
    [System.Serializable]
    [WriteGroup(typeof(LocalToWorld))]
    public struct ECSParameters : ISharedComponentData
    {
        public float MoveSpeed;
        public float MaxSpeed;

        public float ViewRadius;
        public float AvoidRadius;

        public float CohesionForce;
        public float AlignementForce;
        public float AvoidanceForce;
    }
}



