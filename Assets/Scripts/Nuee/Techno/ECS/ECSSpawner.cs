﻿using UnityEngine;

using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Collections;
using Unity.Jobs;
using Unity.Collections.LowLevel.Unsafe;

using UMath = Unity.Mathematics.math;

namespace Nuee.Techno.ECS
{
    public class ECSSpawner : SystemBase
    {
        private BlobAssetStore blobAssetStore;
        private struct SetBoidLocalPositionInWorld : IJobParallelFor
        {
            [NativeDisableContainerSafetyRestriction]
            [NativeDisableParallelForRestriction]
            public ComponentDataFromEntity<LocalToWorld> LocalToWorldFromEntity;

            [NativeDisableContainerSafetyRestriction]
            [NativeDisableParallelForRestriction]
            public ComponentDataFromEntity<ECSBoid> BoidDataFromEntity;

            public NativeArray<Entity> Entities;
            public float3 Spawn;
            public float Range;

            public void Execute(int index)
            {
                var entity = Entities[index];
                float angle = index * UMath.PI * 2 / Entities.Length;
                float3 dir = new float3(UMath.cos(angle), UMath.sin(angle), UMath.sin(angle));
                float3 pos = Spawn + (dir * Range);

                var localToWorld = new LocalToWorld
                {
                    Value = float4x4.TRS(pos, quaternion.LookRotationSafe(dir, math.up()), new float3(1f, 1f, 1f))
                };
                LocalToWorldFromEntity[entity] = localToWorld;
                //Debug.Log($"Entity [{entity.Index}] LocalPosition : {LocalToWorldFromEntity[entity].Position} ");

                var boidData = new ECSBoid
                {
                    Id = entity.Index,
                    Mates = 0,
                    Cohesion = float3.zero,
                    Alignement = float3.zero,
                    Avoidance = float3.zero,
                };
                BoidDataFromEntity[entity] = boidData;
            }
        }

        protected override void OnCreate()
        {
            blobAssetStore = new BlobAssetStore();
        }

        protected override void OnDestroy()
        {
            if (blobAssetStore != null) blobAssetStore.Dispose();
        }

        protected override void OnUpdate()
        {
            Entities.WithStructuralChanges().ForEach((Entity entity, int entityInQueryIndex, in ECSInit init, in LocalToWorld localToWorld) =>
            {
                ECSInit inits = init;
                if (ApplicationManager.Instance && ApplicationManager.Instance.CurrentState == STATE.SETTINGS)
                {
                    GameObject go = ApplicationManager.Instance.PrefabSelected;
                    if (go.TryGetComponent(out Collider col))
                    {
                        col.isTrigger = false;
                        if (!ApplicationManager.Instance.ToggleCol.isOn) UnityEngine.MonoBehaviour.Destroy(col);
                    }

                    inits.Prefab = GameObjectConversionUtility.ConvertGameObjectHierarchy(go,
                        GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, blobAssetStore));
                    inits.Count = ApplicationManager.Instance.BoidsNumber;
                }

                var boidEntities = new NativeArray<Entity>(inits.Count, Allocator.TempJob, NativeArrayOptions.UninitializedMemory);

            // Instantiate les entitées
            EntityManager.AddComponentData(inits.Prefab, new ECSBoid { });
                EntityManager.Instantiate(inits.Prefab, boidEntities);
                Debug.Log($"{inits.Count}");

            // On passe au job les composants de l'entitées que l'on veut étudier ou modifié
            var localToWorldFromEntity = GetComponentDataFromEntity<LocalToWorld>();
                var boidDataFromEntity = GetComponentDataFromEntity<ECSBoid>();
                var setBoidLocalToWorldJob = new SetBoidLocalPositionInWorld
                {
                    LocalToWorldFromEntity = localToWorldFromEntity,
                    BoidDataFromEntity = boidDataFromEntity,
                    Entities = boidEntities,
                    Spawn = float3.zero,
                    Range = inits.SpawnRange,
                };

                Dependency = setBoidLocalToWorldJob.Schedule(inits.Count, 64, Dependency);
                Dependency = boidEntities.Dispose(Dependency);

                ApplicationManager.Instance?.SetUpdate();
            // On détruit cette entité pour éviter de répété le spawn
            EntityManager.DestroyEntity(entity);


            }).Run();
        }
    }
}
