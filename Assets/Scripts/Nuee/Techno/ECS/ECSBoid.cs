﻿
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

namespace Nuee.Techno.ECS
{
    public struct ECSBoid : IComponentData
    {
        public int Id;
        public int Mates;

        public float3 Cohesion;
        public float3 Alignement;
        public float3 Avoidance;
    }
}
