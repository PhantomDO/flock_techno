﻿using Unity.Entities;
using Unity.Transforms;

namespace Nuee.Techno.ECS
{
    [UpdateInGroup(typeof(GameObjectConversionGroup))]
    public class ECSPrefabToEntity : GameObjectConversionSystem
    {
        protected override void OnUpdate()
        {
            Entities.ForEach((BoidParameters parameters) =>
            {
                Entity entity = GetPrimaryEntity(parameters);

            // Ajouter un sharedComponent permet d'avoir une "interface" 
            // permettant de retrouver toutes les entités disposant de ce sharedComponent
            // Vu qu'il est partagé, les entités disposant de ce sharedComponent ont toutes les mêmes valeurs
            // Ils peut y avoir plusieurs type différent, les valeurs sont passé par différents scripts détruit au runtime
            DstEntityManager.AddSharedComponentData(entity, new ECSParameters
                {
                    MoveSpeed = parameters.MoveSpeed,
                    MaxSpeed = parameters.MaxSpeed,
                    ViewRadius = parameters.ViewRadius,
                    AvoidRadius = parameters.AvoidRadius,
                    CohesionForce = parameters.CohesionForce,
                    AlignementForce = parameters.AlignementForce,
                    AvoidanceForce = parameters.AvoidanceForce,
                });

            // On enlève les composant de déplacement et de rotation passé par la conversion en Entity
            // On utilisera le LocalToWorld pour les mouvements
            DstEntityManager.RemoveComponent<Translation>(entity);
                DstEntityManager.RemoveComponent<Rotation>(entity);
            });
        }
    }
}
