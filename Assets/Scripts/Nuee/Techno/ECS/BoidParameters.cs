﻿using UnityEngine;
using Unity.Entities;

namespace Nuee.Techno.ECS
{
    public class BoidParameters : MonoBehaviour
    {
        public float MoveSpeed = 5.0f;
        public float MaxSpeed = 10.0f;

        public float ViewRadius = 2.0f;
        public float AvoidRadius = 1.0f;

        public float CohesionForce = 2.0f;
        public float AlignementForce = 1.0f;
        public float AvoidanceForce = 1.0f;
    }
}
