﻿using UnityEngine;
using System.Collections;
using Unity.Entities;
using Unity.Transforms;

namespace Nuee.Techno.ECS
{
    [RequiresEntityConversion]
    public class GoToEntity : MonoBehaviour, IConvertGameObjectToEntity
    {
        public GameObject prefab = null;
        public Entity prefabToEntity { get; protected set; }

        public virtual void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            if (prefab) prefabToEntity = conversionSystem.GetPrimaryEntity(prefab);
        }
    }
}
