﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using UnityEngine.VFX.Utility;

namespace Nuee.Techno.VFX
{
    public class VFXBoids : MonoBehaviour
    {
        public VisualEffect vfx;

        void Start()
        {
            if (ApplicationManager.Instance && ApplicationManager.Instance?.CurrentState == STATE.SETTINGS)
            {
                vfx.SetInt("Boids Number", ApplicationManager.Instance.BoidsNumber);
            }

            ApplicationManager.Instance?.SetUpdate();
        }
    }
}
