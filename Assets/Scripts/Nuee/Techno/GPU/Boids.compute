﻿// Each #kernel tells which function to compile; you can have many kernels
#pragma kernel CSMain

static const int THREAD_GROUP_SIZE = 4;

// Create a RenderTexture with enableRandomWrite flag and set it
// with cs.SetTexture
//RWTexture2D<float4> Result;
struct Boid
{
    int ID;
    int Mates;

    float3 Cohesion;
    float3 Alignement;
    float3 Avoidance;
    float3 Target;
};

RWStructuredBuffer<Boid> Boids;
int BoidsNumber;
float AvoidRadius;
float ViewRadius;

float TargetRadius;
float TargetForce;

float4 TargetPosition;

[numthreads(THREAD_GROUP_SIZE,1,1)]
void CSMain (uint3 id : SV_DispatchThreadID)
{
    float3 position = Boids[id.x].Cohesion / Boids[id.x].Mates;
    float3 targetPos = TargetPosition.xyz;

    // Center of scene
    if (TargetForce <= 0.0f)
    {
        Boids[id.x].Target = float3(0, 0, 0);
    }
    else
    {
        float3 targetOffset = targetPos - position;
        float sqrTarget = targetOffset.x * targetOffset.x + targetOffset.y * targetOffset.y + targetOffset.z * targetOffset.z;
        float t = sqrTarget / (TargetRadius * TargetRadius);
        if (t < 0.9f) Boids[id.x].Target = float3(0, 0, 0);
        else Boids[id.x].Target = targetOffset * (t * t);
    }

    for (uint idxB = 0; idxB < BoidsNumber; ++idxB)
    {
        if (id.x != idxB)
        {
            Boid boidB = Boids[idxB];
            float3 boidBPosition = boidB.Cohesion / boidB.Mates;
            float3 boidBForward = boidB.Alignement / boidB.Mates;

            float3 heading = (boidBPosition - position);
            float sqrMagnitude = heading.x * heading.x + heading.y * heading.y + heading.z * heading.z;

            if (sqrMagnitude < ViewRadius * ViewRadius)
            {
                Boids[id.x].Mates += 1;
                Boids[id.x].Cohesion += boidBPosition;
                Boids[id.x].Alignement += boidBForward;

                if (sqrMagnitude < AvoidRadius * AvoidRadius)
                {
                    Boids[id.x].Avoidance -= heading / sqrMagnitude;
                }
            }
        }
    }
}
