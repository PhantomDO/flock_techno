﻿using UnityEngine;
using Unity.Jobs;
using UnityEngine.Jobs;
using Unity.Collections;

using Nuee.Parameters;
using Nuee.JobComponent;
using UnityEngine.Rendering;
using System.Collections;
using Unity.Collections.LowLevel.Unsafe;

namespace Nuee.Techno.GPU
{
    public class GPUBoids : Boids
    {
        const int THREAD_GROUP_SIZE = 4;

        public ComputeShader ComputeShader;

        private ComputeBuffer _cptBuffer;
        private int _indexOfKernel;

        protected override void Start()
        {
            _indexOfKernel = ComputeShader.FindKernel("CSMain");
            base.Start();
        }

        protected override void LateUpdate()
        {
            _jobComponent.SetupJobHandle.Complete();
            _cptBuffer.Release();
        }

        private IEnumerator SendDataToShader()
        {
            ComputeShader.SetInt("BoidsNumber", BoidsNumber);
            ComputeShader.SetFloat("AvoidRadius", AvoidRadius);
            ComputeShader.SetFloat("ViewRadius", ViewRadius);
            ComputeShader.SetFloat("TargetRadius", TargetRadius);
            ComputeShader.SetFloat("TargetForce", TargetForce);
            ComputeShader.SetVector("TargetPosition", Target.position);

            _cptBuffer = new ComputeBuffer(BoidsNumber, Boid.Size);  // nombre d'element , size du struct

            if (!IsUsingJobSystem) _cptBuffer.SetData(ABoids);
            else _cptBuffer.SetData(_jobComponent.Boids);

            int threadGroup = Mathf.CeilToInt(BoidsNumber / (float)THREAD_GROUP_SIZE);

            ComputeShader.SetBuffer(_indexOfKernel, "Boids", _cptBuffer);
            ComputeShader.Dispatch(_indexOfKernel, threadGroup, 1, 1);

            _cptBuffer.GetData(ABoids);
            _jobComponent.Boids.CopyFrom(ABoids);

            yield break;
        }

        #region MONO THREAD

        protected override void ExecuteSystem()
        {
            for (int i = 0; i < ABoids.Length; i++)
            {
                ABoids[i].Cohesion = ABoids[i].Avoidance = ATransforms[i].position;
                ABoids[i].Alignement = ATransforms[i].forward;
                ABoids[i].Mates = 1;
            }

            StartCoroutine(SendDataToShader());

            for (int i = 0; i < ABoids.Length; i++) SetNextMovement(i);
        }

        #endregion

        #region MULTI THREAD

        protected override void ExecuteSystemInParallel()
        {
            StartCoroutine(SendDataToShader());

            _jobComponent.SetMovementJob = new SetMovementJob
            {
                Boids = _jobComponent.Boids,
                CohesionForce = CohesionForce,
                AlignementForce = AlignementForce,
                AvoidanceForce = AvoidanceForce,
                TargetForce = TargetForce,
                MoveSpeed = MoveSpeed,
                DeltaTime = Time.deltaTime,
            };

            _jobComponent.SetupJob = new SetupComponentJob
            {
                Boids = _jobComponent.Boids,
                Target = Target.position,
            };

            _jobComponent.SetMovementJobHandle = _jobComponent.SetMovementJob.Schedule(_jobComponent.Transforms);
            _jobComponent.SetupJobHandle = _jobComponent.SetupJob.Schedule(_jobComponent.Transforms, _jobComponent.SetMovementJobHandle);
        }

        #endregion
    }
}
