﻿using UnityEngine;
using Unity.Jobs;
using UnityEngine.Jobs;
using Unity.Collections;

using Nuee.Parameters;
using Nuee.JobComponent;

namespace Nuee.Techno.CPU
{
    public class CPUBoids : Boids
    {
        #region MONO THREAD

        private void CalculateVelocity(int index)
        {
            var boid = ABoids[index];
            var transform = ATransforms[index].transform;

            boid.Mates = 1;
            boid.Cohesion = boid.Avoidance = transform.position;
            boid.Alignement = transform.forward;
            boid.Target = Target.position;

            //Target Offset
            //if (TargetForce <= 0.0f) boid.Target = Vector3.zero;
            //else
            //{
            //    Vector3 targetOffset = boid.Target - transform.position;
            //    float t = targetOffset.sqrMagnitude / (TargetRadius * TargetRadius);
            //    if (t < 0.9f) boid.Target = Vector3.zero;
            //    else boid.Target = targetOffset * (t * t);

            //    //Debug.Log($"Offset : {targetOffset}, t : {t}");
            //}

            for (int i = 0; i < BoidsNumber; i++)
            {
                if (ABoids[i].ID != boid.ID)
                {
                    var trB = ATransforms[i].transform;
                    Vector3 heading = trB.position - transform.position;
                    float sqrDst = Vector3.SqrMagnitude(heading);

                    if (sqrDst < ViewRadius * ViewRadius)
                    {
                        boid.Mates += 1;
                        boid.Cohesion += trB.position;
                        boid.Alignement += trB.forward;

                        if (sqrDst < AvoidRadius * AvoidRadius)
                        {
                            boid.Avoidance -= (heading / sqrDst);
                        }
                    }
                }
            }

            ABoids[index] = boid;
        }

        protected override void ExecuteSystem()
        {
            for (int i = 0; i < BoidsNumber; i++)
            {
                CalculateVelocity(i);
                SetNextMovement(i);
            }
        }

        #endregion

        #region MULTI THREAD

        protected override void ExecuteSystemInParallel()
        {
            _jobComponent.SetupJob = new SetupComponentJob
            {
                Boids = _jobComponent.Boids,
                Target = Target.position,
            };

            _jobComponent.SetVelocityJob = new SetVelocityJob
            {
                Boids = _jobComponent.Boids,
                Target = Target.position,
                ViewRadius = ViewRadius,
                AvoidRadius = AvoidRadius,
                TargetRadius = TargetRadius,
                TargetForce = TargetForce,
            };

            _jobComponent.SetMovementJob = new SetMovementJob
            {
                Boids = _jobComponent.Boids,
                CohesionForce = CohesionForce,
                AlignementForce = AlignementForce,
                AvoidanceForce = AvoidanceForce,
                TargetForce = TargetForce,
                MoveSpeed = MoveSpeed,
                DeltaTime = Time.deltaTime,
            };

            _jobComponent.SetupJobHandle = _jobComponent.SetupJob.Schedule(_jobComponent.Transforms);
            _jobComponent.SetVelocityJobHandle = _jobComponent.SetVelocityJob.Schedule(BoidsNumber, 64, _jobComponent.SetupJobHandle);
            _jobComponent.SetMovementJobHandle = _jobComponent.SetMovementJob.Schedule(_jobComponent.Transforms, _jobComponent.SetVelocityJobHandle);
        }

        #endregion
    }
}