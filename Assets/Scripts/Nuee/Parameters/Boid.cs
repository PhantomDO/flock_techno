﻿using UnityEngine;
using UnityEngine.Jobs;

namespace Nuee.Parameters
{
    [System.Serializable]
    public struct Boid
    {
        public int ID;
        public int Mates;

        public Vector3 Cohesion;
        public Vector3 Alignement;
        public Vector3 Avoidance;
        public Vector3 Target;

        public bool IsGoingToCollide(float boundRadius, float collisionAvoidRadius, LayerMask obstacleMask)
        {
            RaycastHit hit;

            if (Physics.SphereCast(Cohesion, boundRadius, Alignement, out hit, collisionAvoidRadius, obstacleMask))
            {
                return true;
            }

            return false;
        }

        public Vector3 ObstacleRays(Transform transform, float boundRadius, float collisionAvoidRadius, LayerMask obstacleMask, out float dist)
        {
            Vector3[] rayDir = BoidCheck.Directions;

            for (int i = 0; i < rayDir.Length; i++)
            {
                RaycastHit hit;
                Vector3 dir = transform.TransformDirection(rayDir[i]);
                Ray ray = new Ray(transform.position, dir);
                Debug.DrawRay(ray.origin, ray.direction, Color.yellow);

                if (!Physics.SphereCast(ray, boundRadius, out hit, collisionAvoidRadius, obstacleMask))
                {
                    dist = hit.distance;
                    return dir;
                }
            }

            dist = Mathf.Infinity;
            return transform.forward;
        }

        public static int Size
        {
            get
            {
                return sizeof(float) * 3 * 4 + sizeof(int) * 2;
            }
        }
    }
}


