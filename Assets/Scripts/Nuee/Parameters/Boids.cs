﻿using Nuee.JobComponent;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;
using UnityEngine.Jobs;

namespace Nuee.Parameters
{
    public abstract class Boids : MonoBehaviour
    {
        public bool IsUsingJobSystem = false;

        [Header("Settings")]
        public int BoidsNumber = 5;
        public float MoveSpeed = 2f;
        public float ViewRadius = 5f;
        public float AvoidRadius = 2f;
        public float TargetRadius = 20f;
        public Vector2 SpawnRange = Vector2.one;

        [Header("Movement Force")]
        public float CohesionForce = 1f;
        public float AlignementForce = 1f;
        public float AvoidanceForce = 1f;
        public float TargetForce = 1f;

        [Header("Objects")]
        public GameObject Prefab = null;
        public Transform Target = null;

        [Header("Collisions")]
        public LayerMask ObstacleMask;
        public float BoundsRadius = 0.27f;
        public float AvoidCollisionForce = 10f;
        public float CollisionAvoidRadius = 5f;


        public Transform[] ATransforms;
        public Collider[] ACols;
        public Rigidbody[] ARbs;
        public Boid[] ABoids;

        protected BoidJobComponent _jobComponent;

        protected virtual void OnDestroy()
        {
            _jobComponent.Dispose();
        }

        protected virtual void Start()
        {
            if (ApplicationManager.Instance && ApplicationManager.Instance?.CurrentState == STATE.SETTINGS)
            {
                Prefab = ApplicationManager.Instance?.PrefabSelected;
                BoidsNumber = ApplicationManager.Instance.BoidsNumber;
            }

            Spawn(BoidsNumber);
            ApplicationManager.Instance?.SetUpdate();
        }

        protected virtual void Update()
        {
            if (ApplicationManager.Instance && ApplicationManager.Instance?.IsUsingJobs != IsUsingJobSystem)
                IsUsingJobSystem = ApplicationManager.Instance.IsUsingJobs;

            if (!IsUsingJobSystem) ExecuteSystem();
            else ExecuteSystemInParallel();
        }
        protected virtual void LateUpdate()
        {
            if (IsUsingJobSystem)
                _jobComponent.SetMovementJobHandle.Complete();
        }

        #region MONO

        protected virtual void Spawn(int number)
        {
            BoidsNumber = number;
            var spawn = Vector3.zero;

            ATransforms = new Transform[BoidsNumber];
            ARbs = new Rigidbody[BoidsNumber];
            ACols = new Collider[BoidsNumber];
            ABoids = new Boid[BoidsNumber];

            _jobComponent = new BoidJobComponent();
            _jobComponent.Boids = new NativeArray<Boid>(BoidsNumber, Allocator.Persistent, NativeArrayOptions.ClearMemory);

            for (int i = 0; i < BoidsNumber; i++)
            {
                float range = UnityEngine.Random.Range(SpawnRange.x, SpawnRange.y);
                float angle = i * Mathf.PI * 2 / BoidsNumber;
                Vector3 pos = new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), Mathf.Sin(angle));

                spawn += UnityEngine.Random.insideUnitSphere + pos * range;

                GameObject instance = Instantiate(Prefab, spawn, Quaternion.identity/*, transform*/);

                Boid boid = new Boid();
                boid.ID = i;
                boid.Mates = 1;
                boid.Cohesion = boid.Avoidance = instance.transform.position;
                boid.Alignement = instance.transform.forward;
                boid.Target = Target.position;

                ATransforms[i] = instance.transform;
                if (instance.TryGetComponent(out Rigidbody rb)) ARbs[i] = rb;
                if (instance.TryGetComponent(out Collider col))
                {
                    ACols[i] = col;
                    ACols[i].enabled = ApplicationManager.Instance.ToggleCol.isOn;
                    ACols[i].isTrigger = ApplicationManager.Instance.ToggleTrig.isOn;
                }

                ABoids[i] = boid;
            }

            _jobComponent.Transforms = new TransformAccessArray(ATransforms);
            _jobComponent.Boids.CopyFrom(ABoids);
        }

        protected virtual void SetNextMovement(int index)
        {
            var boid = ABoids[index];
            var transform = ATransforms[index].transform;

            var velocity = Vector3.zero;
            
            var target = TargetForce * Vector3.Normalize((boid.Target) - transform.position);
            velocity = target;

            if (boid.Mates >= 1)
            {
                var cohesion = CohesionForce * Vector3.Normalize((transform.position * boid.Mates) - boid.Cohesion);
                var alignement = AlignementForce * Vector3.Normalize(((boid.Alignement / boid.Mates) - transform.forward));
                var avoidance = AvoidanceForce * Vector3.Normalize(boid.Avoidance - transform.position);

                velocity += cohesion;
                velocity += alignement;
                velocity += avoidance;
            }

            foreach(var dir in BoidCheck.Directions) Debug.DrawRay(transform.position, dir, Color.blue);
            if (boid.IsGoingToCollide(BoundsRadius, CollisionAvoidRadius, ObstacleMask))
            {
                var obstacle = transform.position + (boid.ObstacleRays(transform, BoundsRadius, CollisionAvoidRadius, ObstacleMask, out float dist) * dist);
                var obsSteer = Vector3.Normalize(transform.position - obstacle);
                var collisionDir = (obstacle + obsSteer * AvoidCollisionForce) - transform.position;
                velocity += collisionDir;
            }

            var nextHeading = Vector3.Normalize(transform.forward + (Time.deltaTime * (velocity - transform.forward)));

            transform.rotation = Quaternion.LookRotation(nextHeading, Vector3.up);
            transform.position = transform.position + (nextHeading * MoveSpeed * Time.deltaTime);

            ABoids[index] = boid;
        }

        protected virtual void ExecuteSystem()
        {

        }

        #endregion

        #region MULTI

        protected virtual void ExecuteSystemInParallel()
        {

        }

        #endregion



    }
}


