﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class BoidCheck
{
    const int NB_DIRECTIONS = 300;
    public static readonly Vector3[] Directions;

    static BoidCheck()
    {
        Directions = new Vector3[BoidCheck.NB_DIRECTIONS];

        float goldenRatio = (1 + Mathf.Sqrt(5)) / 2;
        float phi = Mathf.PI * 2 * goldenRatio;

        for (int i = 0; i < NB_DIRECTIONS; i++)
        {
            float t = (float)i / NB_DIRECTIONS;
            float radius = Mathf.Acos(1 - 2 * t);
            float theta = phi * i;

            float x = Mathf.Sin(radius) * Mathf.Cos(theta);
            float y = Mathf.Sin(radius) * Mathf.Sin(theta);
            float z = Mathf.Cos(radius);
            Directions[i] = new Vector3(x, y, z);
        }
    }
}
